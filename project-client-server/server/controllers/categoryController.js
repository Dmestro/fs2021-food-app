const {Category} = require('../models/models');
const ApiError = require('../error/ApiError');

class CategoryController {
    //создание категории
    async create(req, res) {
        const {name} = req.body; //из тела запроса извлекаем название категории
        const category = await Category.create({name}) //создаем категорию с помощью ф-ии create
        return res.json({category});
    }
    //получение категорий
    async getAll(req, res) {
        const categories = await Category.findAll(); //findAll возвращает все существующие записи в БД
        return res.json(categories);
    }
}

module.exports = new CategoryController();